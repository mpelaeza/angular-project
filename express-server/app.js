var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("server running on port 3000"));

var ciudades = ["Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile", "Mexico DF"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos = [];
app.get('/my', (req, res, next) => res.json(misDestinos))
app.post('/my', (req, res, next) =>{
	console.log(req.body);
	misDestinos.push(req.body.nuevo)
	res.json(misDestinos)
})


app.get("/api/translation", (req, res, next) => {
	let lang = req.query.lang
	let spanish ={'es': 'Español', 'en': 'Spanish', 'fr': 'Español en Frances'}
	let english ={'es': 'Inglés', 'en': 'English', 'fr': 'Inglés en Frances'}
	let french ={'es': 'Frances', 'en': 'French', 'fr': 'Frances en Frances'}
	res.json([
	{ lang: lang , key: 'hola', value: 'Hola ' + lang },
	// { lang: lang , key: 'spanish', value: spanish[lang] + lang },
	// { lang: lang , key: 'english', value: english[lang] + lang },
	// { lang: lang , key: 'french', value: french[lang] + lang }
	])
})