describe('ventana-principal', () => {
	it('tiene encabezado correcto y en español por defecto', () => {
		cy.visit('http://localhost:4200');
		cy.contains('Sitios para Viajar');
		cy.get('.jumbotron b').should('contain', 'Hola es')
	})

	it('muestra el error si el nombre tiene numeros', () => {
		cy.visit('http://localhost:4200');
		cy.get('form')
      .find('#nombre').type('hola4')
		cy.get('#nombre-error').should('contain', 'No debe contener números')
	})


	it('muestra el error si el nombre  tiene menos de 3 caracteres', () => {
		cy.visit('http://localhost:4200');
		cy.get('form')
      .find('#nombre').type('hol')
		cy.get('#nombre-error').should('contain', 'Debe tener al menos 5 de largo!')
	})

	it('muestra el error si el nombre  tiene menos de 3 caracteres', () => {
		cy.visit('http://localhost:4200');
		cy.get('form')
      .find('#imagenUrl').type('asd').clear()
		cy.get('#url-error').should('contain', 'Requerido')
	})
})
