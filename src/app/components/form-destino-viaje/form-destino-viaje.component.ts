import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn  } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';


import { APP_CONFIG, AppConfig} from '../../app.module'
import {DestinoViaje} from '../../models/destino-viaje.model'

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<DestinoViaje>
	fg: FormGroup
  minLong = 5;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
  	this.onItemAdded = new EventEmitter
  	this.fg = fb.group({
  		nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator, this.nombreMin(this.minLong)
        ])],

  		url: ['', Validators.required], 
  	});

    // this.fg.valueChanges.subscribe((form: any) =>{
    //   console.log(`cambio en el fo rmulario: `, form)
    // })
  }

  

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    let val;
    fromEvent(elemNombre,'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length >= 3),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string)=> ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
        ).subscribe(ajaxResponse => {
          this.searchResults = ajaxResponse.response
        })
  }

  guardar(nombre: string, url: string): boolean{
  	const d = new DestinoViaje(nombre, url, false);
  	this.onItemAdded.emit(d);
    this.fg.reset({nombre: '', url: ''});
  	return false;
  }

  nombreValidator(control: FormControl): {[s: string]: boolean}{
    const value = control.value.toString().trim();
    const l = value.length;

    if(value.match(/\d+/g)){
      return {invalidName: true}
    }
    return null
  }

  nombreMin(minLong: number): ValidatorFn{
    return (control: FormControl): {[s: string]: boolean} | null =>{
      const l = control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        return {minLongName: true}
      }
      return null
    };
  }  

}
