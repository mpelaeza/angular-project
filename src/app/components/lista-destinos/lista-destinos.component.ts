import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import {DestinoViaje} from '../../models/destino-viaje.model'
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction, EliminarDestinoAction, ResetVotesAction } from '../../models/destinos-viajes-state.model'
import { DestinosApiClient } from '../../models/destinos-api-client.model'

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<DestinoViaje>
  destinos: DestinoViaje[];
  updates: string[];
  old_fav: DestinoViaje = null;
  clicks :number;

  constructor(private store: Store<AppState>, private destinosApiClient: DestinosApiClient) { 
  	this.destinos = []
    this.onItemAdded = new EventEmitter();
    this.updates = []
    this.clicks = 0
    this.store.select(state => state.destinos.items[state.destinos.favorito])
    .subscribe(fav => {
      if(fav){
        if(this.old_fav == null || fav.nombre != this.old_fav.nombre){
        this.old_fav = fav;
        this.updates.push(`se ha elegido a ` + fav.nombre)
        }
      }
    })

    this.store.select(state => state.destinos.items)
    .subscribe(act_destinos => { this.destinos = act_destinos} )

    this.store.select(state => state.destinos.tracking_tags)
    .subscribe(tracking_tags => { this.clicks = tracking_tags} )
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje, index: number){
    this.destinosApiClient.add(d, index)
  }

  elegido(d: DestinoViaje, i: number){
    this.destinosApiClient.elegir(d, i)
  }

  borrado(d: DestinoViaje, i: number){
     this.store.dispatch(new EliminarDestinoAction(d, i))
  }

  resetVotes(){
    if(this.destinos.length > 0){
      this.store.dispatch(new ResetVotesAction()) 
    }
  }

}
