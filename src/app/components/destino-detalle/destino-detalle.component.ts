import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from './../../models/destinos-api-client.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module'
import { HttpClient } from '@angular/common/http'

class DestinosApiClientViejo{
	getById(id: String): DestinoViaje{
		console.log('Llamando por la clase vieja!')
		return null
	}
}

interface AppConfig {
	apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig ={
	apiEndpoint: 'http://localhost:3000'
}

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');


@Injectable({
  providedIn: 'root'
})

class DestinosApiClientDecorated extends DestinosApiClient{
	constructor(@Inject(APP_CONFIG) config: AppConfig, store: Store<AppState>, http: HttpClient){
		super(store, config, http);
	}

	getById(id: String): DestinoViaje{
		console.log('Llamando por la clase decorada!')
		return super.getById(id)
	}
}

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.scss'],
  styles: [
      `
        mgl-map {
          height: 100%;
          width: 100%;
        }
      `
    ],
  providers: [ 
  	{ provide: APP_CONFIG, useValue: APP_CONFIG_VALUE}, 
  	{ provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
  	{ provide: DestinosApiClientViejo, useExisting: DestinosApiClient }
  ]
})

export class DestinoDetalleComponent implements OnInit {
	destino: DestinoViaje;
	style = {
		sources:{
			world: {
				type: 'geojson',
				data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
			}
		},
		version: 8,
		layers: [{
			'id': 'countries',
			'type': 'fill',
			'source': 'world',
			'layout': {},
			'paint': {'fill-color': '#6F788A'},
		}]
	}

  constructor(private route :ActivatedRoute, private destinosApiClient :DestinosApiClientViejo) { }

  ngOnInit(): void {
  	let id = this.route.snapshot.paramMap.get('nombre');
  	this.destino = this.destinosApiClient.getById(id)
  }

}
