import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import {DestinoViaje} from '../../models/destino-viaje.model'
import { Store } from '@ngrx/store';
import {AppState} from '../../app.module'
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model'
import {trigger, state, style, transition, animate } from '@angular/animations'

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss'],
  animations: [
  trigger('esFavorito',[
  state('estadoFavorito', style({
    backgroundColor: 'PaleTurquoise'
  })),
  state('estadoNoFavorito', style({
    backgroundColor: 'WhiteSmoke'
  })),
  transition('estadoNoFavorito => estadoFavorito', [
    animate('3s')
    ]),
  transition('estadoFavorito => estadoNoFavorito', [
    animate('1s')
    ]),
  ])]
})
export class DestinoViajeComponent implements OnInit {
	@Input() destino: DestinoViaje;
  @Input('idx') index: number;
	@HostBinding('attr.class') cssClass="col-md-4 destino-viaje";
	@Output() clicked: EventEmitter<DestinoViaje>;
  @Output() deleted: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) { 
  	this.clicked = new EventEmitter();
    this.deleted = new EventEmitter();
  }

  ngOnInit(): void {
  }

  ir(){
  	this.clicked.emit(this.destino);
  	return false
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino, this.index))
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino, this.index))
    return false;
  }

  eliminar(){
    this.deleted.emit(this.destino)
  }

}
