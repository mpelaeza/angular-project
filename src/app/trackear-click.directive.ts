import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs'
import { Store } from '@ngrx/store';
import { AppState } from './app.module';
import { contarClicks } from './models/destinos-viajes-state.model'

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
	private element :HTMLInputElement

  constructor(private elRef: ElementRef, private store: Store<AppState>) {
  	this.element = elRef.nativeElement
  	fromEvent(this.element, 'click').subscribe(evento => this.track(evento))
   }

   track(evento: Event) :void{
     this.store.dispatch(new contarClicks())
   	const elemTags = this.element.attributes.getNamedItem(`data-trackear-tag`).value.split(' ')

   	console.log(`|||||||||||||||||| track evento ${elemTags}`)
   }

}
