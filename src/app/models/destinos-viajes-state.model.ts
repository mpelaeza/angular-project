import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model';
import { HttpClientModule, HttpHeaders, HttpRequest, HttpClient, HttpResponse } from '@angular/common/http'

// ESTADO
export interface DestinosViajesState{
	items: DestinoViaje[];
	loading: boolean;
	favorito: number;
	index: number,
	tracking_tags: number
}

export const initializeDestinosViajesState = function(){
	return{
		items: [],
		loading: false,
		favorito: null,
		index : 1,
		tracking_tags: 0
	}
}

// ACCIONES
export enum DestinosViajesActionTypes {
	 NUEVO_DESTINO = '[DESTINOS VIAJES] NUEVO',
	 ELIMINAR_DESTINO = '[DESTINOS VIAJES] ELIMINAR',
	 ELEJIDO_FAVORITO = '[DESTINOS VIAJES] FAVORITO',
	 VOTE_UP ='[DESTINO VIAJE] VOTE UP',
	 VOTE_DOWN ='[DESTINO VIAJE] VOTE DOWN',
	 RESET_VOTES ='[LISTA DESTINOS] RESET VOTES',
	 INIT_DATA= 'INIT DATA',
	 TRACKING_TAGS= 'TRACKING TAGS'
}

export class NuevoDestinoAction implements Action{
	type = DestinosViajesActionTypes.NUEVO_DESTINO;
	constructor(public destino: DestinoViaje, public index: number){}
}

export class ElegidoFavoritoAction implements Action{
	type = DestinosViajesActionTypes.ELEJIDO_FAVORITO;
	constructor(public destino: DestinoViaje, public index: number){}
}

export class EliminarDestinoAction implements Action{
	type = DestinosViajesActionTypes.ELIMINAR_DESTINO;
	constructor(public destino: DestinoViaje, public index: number){}
}

export class VoteUpAction implements Action{
	type = DestinosViajesActionTypes.VOTE_UP;
	constructor(public destino: DestinoViaje, public index: number){}
}

export class VoteDownAction implements Action{
	type = DestinosViajesActionTypes.VOTE_DOWN;
	constructor(public destino: DestinoViaje, public index: number){}
}

export class ResetVotesAction implements Action{
	type = DestinosViajesActionTypes.RESET_VOTES;
	constructor(){}
}

export class InitMyDataAction implements Action{
	type = DestinosViajesActionTypes.INIT_DATA;
	constructor(public destinos: string[]){}
}

export class contarClicks implements Action{
	type = DestinosViajesActionTypes.TRACKING_TAGS;
	constructor(){}
}



export type DestinosViajesActions = NuevoDestinoAction | EliminarDestinoAction 
                                    | ElegidoFavoritoAction | VoteUpAction
                                    | VoteDownAction | ResetVotesAction
                                    | InitMyDataAction | contarClicks

// REDUCERS
export function reducerDestinosViajes(
	state: DestinosViajesState,
	action: DestinosViajesActions
	):DestinosViajesState{
	switch(action.type){
		case DestinosViajesActionTypes.NUEVO_DESTINO:{
			var new_index = state.index + 1;
			return{
				...state,
				items: [...state.items, (action as NuevoDestinoAction).destino],
				index: new_index
			}
		}
		case DestinosViajesActionTypes.ELEJIDO_FAVORITO:{
			var {favorito, items} = {...state}
			var fav_index = (action as ElegidoFavoritoAction).index;
			var fav = (action as ElegidoFavoritoAction).destino;
			var old_fav = items[favorito]
			var new_fav = new DestinoViaje(fav.nombre, fav.imagenUrl, true, fav.votes)
			items = [...items.slice(0, fav_index), new_fav, ...items.slice(fav_index + 1)]
			if(old_fav){
				old_fav = new DestinoViaje(old_fav.nombre, old_fav.imagenUrl, false, old_fav.votes)
				items = [...items.slice(0, favorito), old_fav, ...items.slice(favorito + 1)]
			}
			return{
				...state,
				items: items,
				favorito: fav_index
			}
		}
		case DestinosViajesActionTypes.ELIMINAR_DESTINO:{
			var {favorito, items, index} = {...state}
			var deleted_index = (action as NuevoDestinoAction).index
			var deleted_item = items[deleted_index]
			if(deleted_item.isSelected()){
				return{
					...state,
					items: [...items.slice(0, deleted_index), ...items.slice(deleted_index + 1)],
					index: index -1,
					favorito: null
				}

			}
			else{
				return{
					...state,
					items: [...items.slice(0, deleted_index), ...items.slice(deleted_index + 1)],
					index: index -1
				}	
			}
		}
		case DestinosViajesActionTypes.VOTE_UP:{
			var {items} = {...state}
			var vote_up_index = (action as VoteUpAction).index
			var destino = (action as VoteUpAction).destino
			var voted_item = new DestinoViaje(destino.nombre, destino.imagenUrl, 
				                                destino.isSelected(), destino.votes + 1)
			return{
				...state,
				items:  [...items.slice(0, vote_up_index), voted_item, ...items.slice(vote_up_index + 1)]
			}
		}
		case DestinosViajesActionTypes.VOTE_DOWN:{
			var {items} = {...state}
			var vote_down_index = (action as VoteUpAction).index
			var destino = (action as VoteUpAction).destino
			var voted_item = new DestinoViaje(destino.nombre, destino.imagenUrl, 
				                                destino.isSelected(), destino.votes - 1)
			return{
				...state,
				items:  [...items.slice(0, vote_down_index), voted_item, ...items.slice(vote_down_index + 1)]
			}
		}
		case DestinosViajesActionTypes.RESET_VOTES:{
			var {items} = {...state}
			var new_items: DestinoViaje[] = []
			items.forEach(destino => new_items.push(new DestinoViaje(destino.nombre, destino.imagenUrl, 
				                                destino.isSelected(), 0)))
			return{
				...state,
				items: new_items
			}
		}
		case DestinosViajesActionTypes.INIT_DATA:{
			const destinos: string[] = (action as InitMyDataAction).destinos
			return{
				...state,
				items: destinos.map((d) => new DestinoViaje(d, 'test', false, 0)),
				index: destinos.length
			}
		}
		case DestinosViajesActionTypes.TRACKING_TAGS:{
			const { tracking_tags } = {...state}
			return{
				...state,
				tracking_tags: tracking_tags + 1
			}
		}
	}
	return state;
}

// EFECTS
@Injectable()
export class DestinosViajesEffects{
	@Effect()
	nuevoAgregados$: Observable<Action> = this.actions$.pipe(
		ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
		map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino, action.index))
	);
	constructor(private actions$: Actions){}
}