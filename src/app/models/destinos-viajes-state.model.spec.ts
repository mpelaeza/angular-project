import { DestinosViajesState, 
	       reducerDestinosViajes, 
	       initializeDestinosViajesState, 
	       DestinosViajesEffects, 
	       InitMyDataAction,
	       NuevoDestinoAction,
	       ElegidoFavoritoAction,
	       EliminarDestinoAction,
	       VoteUpAction,
	       VoteDownAction,
	       ResetVotesAction,
	       contarClicks } from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
	it('should reduce init data', () =>  {
		const prevState: DestinosViajesState = initializeDestinosViajesState();
		const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items.length).toEqual(2);
		expect(newState.items[0].nombre).toEqual('destino 1');
	});

	it('should reduce new item added', () => {
		const prevState: DestinosViajesState = initializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Barcelona', 'asdfg', false, 0), 0);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items.length).toEqual(1);
		expect(newState.items[0].nombre).toEqual('Barcelona');
	});

	it('should reduce item chosen favorite', () => {
		let prevState: DestinosViajesState = initializeDestinosViajesState();
		let old_action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2', 'destino 3']);
		prevState = reducerDestinosViajes(prevState, old_action) 
		const fav_destino = prevState.items[1]
		const action: ElegidoFavoritoAction = new ElegidoFavoritoAction(fav_destino, 1);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.favorito).toEqual(1);
		expect(newState.items[1].isSelected()).toEqual(true);
	});

	it('should reduce item deleted', () => {
		let prevState: DestinosViajesState = initializeDestinosViajesState();
		let old_action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2', 'destino 3']);
		prevState = reducerDestinosViajes(prevState, old_action) 
		const del = prevState.items[1]
		const action: EliminarDestinoAction = new EliminarDestinoAction(del, 1);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items.length).toEqual(2);
	});

	it('should reduce item voted up', () => {
		let prevState: DestinosViajesState = initializeDestinosViajesState();
		let old_action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2', 'destino 3']);
		prevState = reducerDestinosViajes(prevState, old_action) 
		const voted_up = prevState.items[1]
		const action: VoteUpAction = new VoteUpAction(voted_up, 1);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items[1].votes).toEqual(1);
	})

	it('should reduce item voted down', () => {
		let prevState: DestinosViajesState = initializeDestinosViajesState();
		let old_action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2', 'destino 3']);
		prevState = reducerDestinosViajes(prevState, old_action) 
		const voted_down = prevState.items[1]
		const action: VoteDownAction = new VoteDownAction(voted_down, 1);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items[1].votes).toEqual(-1);
	})

	it('should reduce item voted down', () => {
		let prevState: DestinosViajesState = initializeDestinosViajesState();
		let old_action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2', 'destino 3']);
		prevState = reducerDestinosViajes(prevState, old_action) 
		const voted_down = prevState.items[1]
		let old_action2: VoteDownAction = new VoteDownAction(voted_down, 1);
		prevState = reducerDestinosViajes(prevState, old_action2) 
		const action: ResetVotesAction = new ResetVotesAction();
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.items[1].votes).toEqual(0);
	})

	it('should reduce contar clicks', () => {
		const prevState: DestinosViajesState = initializeDestinosViajesState();
		const action: contarClicks = new contarClicks();
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action)
		expect(newState.tracking_tags).toEqual(1);
	})
});

