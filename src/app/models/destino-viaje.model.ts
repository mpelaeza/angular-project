export class DestinoViaje{
	protected selected: boolean;
	public servicios: string[]
	public id: number;

	constructor(public nombre:string, public imagenUrl:string, selected: boolean,public votes: number = 0){
		this.servicios= ['piscina', 'desayuno']
		this.selected = selected 
		this.votes = votes
	}

	isSelected() {
		return this.selected;
	}

	setSelected(s: boolean){
		console.log(this)
		this.selected = s;
	}

	voteUp(){
		this.votes += 1;
	}

	voteDown(){
		this.votes -= 1;
	}
}